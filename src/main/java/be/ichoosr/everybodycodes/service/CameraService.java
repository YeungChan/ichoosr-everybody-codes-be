package be.ichoosr.everybodycodes.service;

import be.ichoosr.everybodycodes.domain.Camera;
import be.ichoosr.everybodycodes.repository.CameraRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

@Service
@AllArgsConstructor
public class CameraService {
    private final CameraRepository cameraRepository;

    public void saveCameraData() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("C:/Users/yeung/Downloads/cameras-defb.csv"));
            String line;
            reader.readLine();
            while ((line = reader.readLine()) != null) {
                if (!line.startsWith("ERROR")) {
                    String[] data = line.split(";");
                    Camera camera = new Camera(data[0], Float.parseFloat(data[1]), Float.parseFloat(data[2]));
                    cameraRepository.save(camera);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Camera> getCameraByName(String cameraName) {
        return cameraRepository.findAllByCameraNameContaining(cameraName);
    }

    public List<Camera> getAllCameras() {
        return cameraRepository.findAll();
    }
}

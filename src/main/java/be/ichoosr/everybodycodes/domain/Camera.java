package be.ichoosr.everybodycodes.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Camera {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    private String cameraName;
    private float latitude;
    private float longitude;

    public Camera(String cameraName, float latitude, float longitude) {
        this.cameraName = cameraName;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    @Override
    public String toString() {
        return String.format("%s | %f | %f", cameraName, longitude, latitude);
    }
}

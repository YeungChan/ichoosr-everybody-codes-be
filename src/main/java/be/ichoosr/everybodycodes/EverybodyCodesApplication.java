package be.ichoosr.everybodycodes;

import be.ichoosr.everybodycodes.domain.Camera;
import be.ichoosr.everybodycodes.repository.CameraRepository;
import be.ichoosr.everybodycodes.service.CameraService;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@SpringBootApplication
@AllArgsConstructor
public class EverybodyCodesApplication implements CommandLineRunner {
    private final CameraService cameraService;

    public static void main(String[] args) {
        SpringApplication.run(EverybodyCodesApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("----- Welcome -----");
        System.out.println("Search camera: ");
        String input = scanner.nextLine();
        List<Camera> cameras = cameraService.getCameraByName(input);
        cameras.forEach(System.out::println);

    }

}

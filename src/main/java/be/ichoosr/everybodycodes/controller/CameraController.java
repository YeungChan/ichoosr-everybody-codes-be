package be.ichoosr.everybodycodes.controller;

import be.ichoosr.everybodycodes.domain.Camera;
import be.ichoosr.everybodycodes.service.CameraService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cameras")
@AllArgsConstructor
public class CameraController {
    private final CameraService cameraService;

    @GetMapping("/init")
    public void init() {
        cameraService.saveCameraData();
    }

    @GetMapping
    public List<Camera> getAllCameras() {
        return cameraService.getAllCameras();
    }

    @GetMapping("/{name}")
    public List<Camera> getCamerasContainingName(@PathVariable String name) {
        return cameraService.getCameraByName(name);
    }
}
